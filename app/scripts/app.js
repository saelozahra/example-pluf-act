'use strict';

/**
 * @ngdoc overview
 * @name examplePlufActApp
 * @description
 * # examplePlufActApp
 *
 * Main module of the application.
 */
angular
  .module('examplePlufActApp', [
    'ngAnimate',
    'ngAria',
    'ngMessages',
    'ngSanitize',
	'ngMaterial',
	'pluf'
  ]);
