'use strict';

/**
 * @ngdoc function
 * @name examplePlufActApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the examplePlufActApp
 */
angular.module('examplePlufActApp').controller('MainCtrl', function($scope, $act, $usr) {
	
//	$act.menu('header').then(function(hmenu){
//		$scope.headerMenu = hmenu;
//	});


$act.command({
	id: 'user.login',
	label: 'login',
	icon: 'enter',
	tags: ['user', 'login']
});

$act.handler({
	command: 'user.login',
	handle: function(credential){
		return $usr.login(credential);
	}
});
	
function runCommand(){
	$act.execute('user.login',{
		login: 'admin',
		password: 'admin'
	});
}
  	
	$scope.act = $act._commands;
	$scope.runCommand = runCommand;

});
