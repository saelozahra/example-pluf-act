'use strict';

angular.module('examplePlufActApp')

/**
 * @ngdoc runs
 * @description
 */
.run(function($act, $usr) {

	$act.command({
		id : 'user.login',
		label : 'login',
		icon : 'enter',
		tags : [ 'user', 'login' ]
	});

	$act.handler({
		command : 'user.login',
		id : 'user.login',
		handle : function(credential) {
			alert('Login handler');
			return $usr.login(credential);
		}
	});

});